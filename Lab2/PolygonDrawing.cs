﻿using System;
using System.Drawing;


namespace Lab2
{
    public class PolygonDrawing
    {
        public Polygon Polygon;
        public Color Color;
        public int SizePercentage;
        public float AngleRotated;
        public PointF Size;


        public PolygonDrawing(int width, int height)
        {
            this.Color = Color.BlueViolet;
            this.SizePercentage = 100;
            this.AngleRotated = 0.0F;
            this.Size = new PointF(width, height);

            this.Polygon = new Polygon(
                new PointF(width / 2.0F, height / 2.0F),
                new PointF(0.0F, 0.0F),
                Math.Min(height, width),
                this.SizePercentage,
                5
            );
        }


        public void Draw(Graphics g)
        {
            var brush = new SolidBrush(Color);
            g.FillPolygon(brush, Polygon.Vertices);
            brush.Dispose();
        }


    }
}
