﻿using System;
using System.Windows.Forms;

namespace Lab2
{
    public partial class ScalePicker : UserControl, ISettingsMutator
    {
        public ScalePicker()
        {
            InitializeComponent();
        }


        public void ApplySettings(PolygonDrawing drawing)
        {
            scaleBar.Value = drawing.SizePercentage;
        }


        public void ApplyToPolygon(ref PolygonDrawing old)
        {
            var newSize = scaleBar.Value;
            var oldSize = old.SizePercentage;
            var rescaledPolygon = old.Polygon.Resized(oldSize, newSize);

            old.SizePercentage = newSize;
            old.Polygon = rescaledPolygon;
        }

        public void OnApply(EventHandler handler)
        {
            resizeButton.Click += handler;
        }
    }
}
