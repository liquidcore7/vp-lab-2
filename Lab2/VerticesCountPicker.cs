﻿using System;
using System.Windows.Forms;

namespace Lab2
{
    public partial class VerticesCountPicker : UserControl, ISettingsMutator
    {
        public VerticesCountPicker()
        {
            InitializeComponent();
        }

        public void ApplySettings(PolygonDrawing drawing)
        {
            countPickerBox.Value = drawing.Polygon.Vertices.Length;
        }

        public void ApplyToPolygon(ref PolygonDrawing old)
        {
            var newCount = Convert.ToInt32(countPickerBox.Value);
            old.Polygon = new Polygon(old.Polygon.Center, old.Polygon.GetMaxRadius(), newCount);
        }

        public void OnApply(EventHandler handler)
        {
            applyCountButton.Click += handler;
        }
    }
}
