﻿namespace Lab2
{
    partial class ScalePicker
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.scalePickerLayout = new System.Windows.Forms.TableLayoutPanel();
            this.scaleLabel = new System.Windows.Forms.Label();
            this.scaleBar = new System.Windows.Forms.TrackBar();
            this.resizeButton = new System.Windows.Forms.Button();
            this.scalePickerLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scaleBar)).BeginInit();
            this.SuspendLayout();
            // 
            // scalePickerLayout
            // 
            this.scalePickerLayout.ColumnCount = 1;
            this.scalePickerLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.scalePickerLayout.Controls.Add(this.scaleLabel, 0, 0);
            this.scalePickerLayout.Controls.Add(this.scaleBar, 0, 1);
            this.scalePickerLayout.Controls.Add(this.resizeButton, 0, 2);
            this.scalePickerLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scalePickerLayout.Location = new System.Drawing.Point(0, 0);
            this.scalePickerLayout.Name = "scalePickerLayout";
            this.scalePickerLayout.RowCount = 3;
            this.scalePickerLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.scalePickerLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.scalePickerLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.scalePickerLayout.Size = new System.Drawing.Size(150, 150);
            this.scalePickerLayout.TabIndex = 0;
            // 
            // scaleLabel
            // 
            this.scaleLabel.AutoSize = true;
            this.scaleLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scaleLabel.Location = new System.Drawing.Point(3, 0);
            this.scaleLabel.Name = "scaleLabel";
            this.scaleLabel.Size = new System.Drawing.Size(144, 30);
            this.scaleLabel.TabIndex = 0;
            this.scaleLabel.Text = "Figure scale";
            // 
            // scaleBar
            // 
            this.scaleBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scaleBar.LargeChange = 2;
            this.scaleBar.Location = new System.Drawing.Point(3, 33);
            this.scaleBar.Maximum = 100;
            this.scaleBar.Minimum = 10;
            this.scaleBar.Name = "scaleBar";
            this.scaleBar.Size = new System.Drawing.Size(144, 39);
            this.scaleBar.TabIndex = 1;
            this.scaleBar.TickFrequency = 10;
            this.scaleBar.Value = 10;
            // 
            // resizeButton
            // 
            this.resizeButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resizeButton.Location = new System.Drawing.Point(3, 78);
            this.resizeButton.Name = "resizeButton";
            this.resizeButton.Size = new System.Drawing.Size(144, 69);
            this.resizeButton.TabIndex = 2;
            this.resizeButton.Text = "Resize";
            this.resizeButton.UseVisualStyleBackColor = true;
            // 
            // ScalePicker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.scalePickerLayout);
            this.Name = "ScalePicker";
            this.scalePickerLayout.ResumeLayout(false);
            this.scalePickerLayout.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scaleBar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel scalePickerLayout;
        private System.Windows.Forms.Label scaleLabel;
        private System.Windows.Forms.TrackBar scaleBar;
        private System.Windows.Forms.Button resizeButton;
    }
}
