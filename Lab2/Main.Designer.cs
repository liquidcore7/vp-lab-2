﻿namespace Lab2
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.globalLayout = new System.Windows.Forms.TableLayoutPanel();
            this.polygonDrawingBox = new System.Windows.Forms.PictureBox();
            this.verticesCountPicker = new Lab2.VerticesCountPicker();
            this.rectangleColorPicker = new Lab2.ColorPicker();
            this.scalePicker = new Lab2.ScalePicker();
            this.globalLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.polygonDrawingBox)).BeginInit();
            this.SuspendLayout();
            // 
            // globalLayout
            // 
            this.globalLayout.ColumnCount = 2;
            this.globalLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.globalLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.globalLayout.Controls.Add(this.polygonDrawingBox, 0, 0);
            this.globalLayout.Controls.Add(this.verticesCountPicker, 1, 0);
            this.globalLayout.Controls.Add(this.rectangleColorPicker, 1, 2);
            this.globalLayout.Controls.Add(this.scalePicker, 1, 1);
            this.globalLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.globalLayout.Location = new System.Drawing.Point(0, 0);
            this.globalLayout.Name = "globalLayout";
            this.globalLayout.RowCount = 3;
            this.globalLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.globalLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 22.22222F));
            this.globalLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 44.44444F));
            this.globalLayout.Size = new System.Drawing.Size(800, 450);
            this.globalLayout.TabIndex = 0;
            // 
            // polygonDrawingBox
            // 
            this.polygonDrawingBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.polygonDrawingBox.Location = new System.Drawing.Point(3, 3);
            this.polygonDrawingBox.Name = "polygonDrawingBox";
            this.globalLayout.SetRowSpan(this.polygonDrawingBox, 3);
            this.polygonDrawingBox.Size = new System.Drawing.Size(634, 444);
            this.polygonDrawingBox.TabIndex = 0;
            this.polygonDrawingBox.TabStop = false;
            // 
            // verticesCountPicker
            // 
            this.verticesCountPicker.Dock = System.Windows.Forms.DockStyle.Fill;
            this.verticesCountPicker.Location = new System.Drawing.Point(643, 3);
            this.verticesCountPicker.Name = "verticesCountPicker";
            this.verticesCountPicker.Size = new System.Drawing.Size(154, 143);
            this.verticesCountPicker.TabIndex = 1;
            // 
            // rectangleColorPicker
            // 
            this.rectangleColorPicker.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rectangleColorPicker.Location = new System.Drawing.Point(643, 251);
            this.rectangleColorPicker.Name = "rectangleColorPicker";
            this.rectangleColorPicker.Size = new System.Drawing.Size(154, 196);
            this.rectangleColorPicker.TabIndex = 2;
            // 
            // scalePicker
            // 
            this.scalePicker.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scalePicker.Location = new System.Drawing.Point(643, 152);
            this.scalePicker.Name = "scalePicker";
            this.scalePicker.Size = new System.Drawing.Size(154, 93);
            this.scalePicker.TabIndex = 3;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.globalLayout);
            this.Name = "Main";
            this.Text = "Lab 2";
            this.globalLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.polygonDrawingBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel globalLayout;
        private System.Windows.Forms.PictureBox polygonDrawingBox;
        private VerticesCountPicker verticesCountPicker;
        private ColorPicker rectangleColorPicker;
        private ScalePicker scalePicker;
    }
}

