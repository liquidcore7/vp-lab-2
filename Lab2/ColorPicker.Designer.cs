﻿namespace Lab2
{
    partial class ColorPicker
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.rectanleColorLabel = new System.Windows.Forms.Label();
            this.colorPickerDialog = new System.Windows.Forms.ColorDialog();
            this.applyColorButton = new System.Windows.Forms.Button();
            this.pickedColorBox = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pickedColorBox)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.rectanleColorLabel, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.applyColorButton, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.pickedColorBox, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(150, 150);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // rectanleColorLabel
            // 
            this.rectanleColorLabel.AutoSize = true;
            this.rectanleColorLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rectanleColorLabel.Location = new System.Drawing.Point(3, 0);
            this.rectanleColorLabel.Name = "rectanleColorLabel";
            this.rectanleColorLabel.Size = new System.Drawing.Size(144, 22);
            this.rectanleColorLabel.TabIndex = 0;
            this.rectanleColorLabel.Text = "Rectangle color";
            // 
            // colorPickerDialog
            // 
            this.colorPickerDialog.AnyColor = true;
            this.colorPickerDialog.ShowHelp = true;
            this.colorPickerDialog.SolidColorOnly = true;
            // 
            // applyColorButton
            // 
            this.applyColorButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.applyColorButton.Location = new System.Drawing.Point(3, 115);
            this.applyColorButton.Name = "applyColorButton";
            this.applyColorButton.Size = new System.Drawing.Size(144, 32);
            this.applyColorButton.TabIndex = 1;
            this.applyColorButton.Text = "Apply color";
            this.applyColorButton.UseVisualStyleBackColor = true;
            // 
            // pickedColorBox
            // 
            this.pickedColorBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pickedColorBox.Location = new System.Drawing.Point(3, 25);
            this.pickedColorBox.Name = "pickedColorBox";
            this.pickedColorBox.Size = new System.Drawing.Size(144, 84);
            this.pickedColorBox.TabIndex = 2;
            this.pickedColorBox.TabStop = false;
            // 
            // ColorPicker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "ColorPicker";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pickedColorBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label rectanleColorLabel;
        private System.Windows.Forms.Button applyColorButton;
        private System.Windows.Forms.ColorDialog colorPickerDialog;
        private System.Windows.Forms.PictureBox pickedColorBox;
    }
}
