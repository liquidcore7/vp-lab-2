﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Lab2
{
    public partial class Main : Form
    {
        private PolygonDrawing polygonDrawing;

        private Point? prevDragPoint;

        public Main()
        {
            InitializeComponent();

            this.polygonDrawing = new PolygonDrawing(polygonDrawingBox.Width, polygonDrawingBox.Height);

            ISettingsMutator[] settingsMutators = new ISettingsMutator[] { 
                verticesCountPicker, 
                rectangleColorPicker,
                scalePicker
            };

            foreach (var settingsMutator in settingsMutators)
            {
                settingsMutator.ApplySettings(polygonDrawing);
                settingsMutator.OnApply((sender, args) =>
                {
                    settingsMutator.ApplyToPolygon(ref polygonDrawing);
                    polygonDrawingBox.Invalidate();
                });
            }

            polygonDrawingBox.Paint += new PaintEventHandler((sender, args) => polygonDrawing.Draw(args.Graphics));
            polygonDrawingBox.Resize += new EventHandler((sender, args) => {
                (sender as PictureBox)?.Invalidate();
            });


            polygonDrawingBox.MouseWheel += new MouseEventHandler((object sender, MouseEventArgs args) =>
            {
                double angle = Math.PI * args.Delta / 360.0;
                polygonDrawing.Polygon = polygonDrawing.Polygon.RotatedBy((float) angle);
                (sender as PictureBox)?.Invalidate();
            });


            polygonDrawingBox.MouseDown += new MouseEventHandler((object sender, MouseEventArgs args) =>
            {
                var clickedPoint = args.Location;

                if (args.Button == MouseButtons.Left && polygonDrawing.Polygon.ContainsPoint(clickedPoint))
                {
                    prevDragPoint = args.Location;
                }
            });

            polygonDrawingBox.MouseMove += new MouseEventHandler((object sender, MouseEventArgs args) =>
            {
                var newPoint = args.Location;
                if (prevDragPoint.HasValue)
                {
                    var prevPoint = prevDragPoint.Value;
                    polygonDrawing.Polygon = polygonDrawing.Polygon.Moved(prevPoint, newPoint);
                    prevDragPoint = newPoint;
                    polygonDrawingBox.Invalidate();
                }
            });

            polygonDrawingBox.MouseUp += new MouseEventHandler((object sender, MouseEventArgs args) =>
            {
                if (prevDragPoint.HasValue)
                {
                    prevDragPoint = null;
                }
            });
        }

    }
}
