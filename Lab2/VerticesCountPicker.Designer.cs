﻿namespace Lab2
{
    partial class VerticesCountPicker
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.verticesCountLabel = new System.Windows.Forms.Label();
            this.countPickerBox = new System.Windows.Forms.NumericUpDown();
            this.applyCountButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.countPickerBox)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.verticesCountLabel, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.countPickerBox, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.applyCountButton, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(150, 150);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // verticesCountLabel
            // 
            this.verticesCountLabel.AutoSize = true;
            this.verticesCountLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.verticesCountLabel.Location = new System.Drawing.Point(3, 0);
            this.verticesCountLabel.Name = "verticesCountLabel";
            this.verticesCountLabel.Size = new System.Drawing.Size(144, 37);
            this.verticesCountLabel.TabIndex = 0;
            this.verticesCountLabel.Text = "Vertices count";
            // 
            // countPickerBox
            // 
            this.countPickerBox.AutoSize = true;
            this.countPickerBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.countPickerBox.Location = new System.Drawing.Point(3, 40);
            this.countPickerBox.Name = "countPickerBox";
            this.countPickerBox.Size = new System.Drawing.Size(144, 22);
            this.countPickerBox.TabIndex = 1;
            // 
            // applyCountButton
            // 
            this.applyCountButton.AutoSize = true;
            this.applyCountButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.applyCountButton.Location = new System.Drawing.Point(3, 77);
            this.applyCountButton.Name = "applyCountButton";
            this.applyCountButton.Size = new System.Drawing.Size(144, 70);
            this.applyCountButton.TabIndex = 2;
            this.applyCountButton.Text = "Apply";
            this.applyCountButton.UseVisualStyleBackColor = true;
            // 
            // VerticesCountPicker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "VerticesCountPicker";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.countPickerBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label verticesCountLabel;
        private System.Windows.Forms.NumericUpDown countPickerBox;
        private System.Windows.Forms.Button applyCountButton;
    }
}
