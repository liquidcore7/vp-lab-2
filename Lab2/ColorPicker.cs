﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Lab2
{
    public partial class ColorPicker : UserControl, ISettingsMutator
    {
        public ColorPicker()
        {
            InitializeComponent();
            pickedColorBox.Click += new EventHandler((sender, args) =>
            {
                if (colorPickerDialog.ShowDialog() == DialogResult.OK)
                {
                    handleColorChange(colorPickerDialog.Color);
                }
            });
            applyColorButton.Click += new EventHandler((sender, args) => {
                handleColorChange(colorPickerDialog.Color);
            });
        }

        public void ApplySettings(PolygonDrawing drawing)
        {
            handleColorChange(drawing.Color);
            colorPickerDialog.Color = drawing.Color;
        }

        public void ApplyToPolygon(ref PolygonDrawing old)
        {
            old.Color = colorPickerDialog.Color;
        }


        public void OnApply(EventHandler handler)
        {
            applyColorButton.Click += handler;
        }


        private void handleColorChange(Color newColor)
        {
            pickedColorBox.BackColor = newColor;
        }
    }
}
