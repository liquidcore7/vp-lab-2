﻿using System;
using System.Drawing;

namespace Lab2
{
    public static class PointUtils
    {
        public static PointF CalcPairwise(PointF l, PointF r, Func<float, float, float> binaryOp)
        {
            return new PointF(binaryOp(l.X, r.X), binaryOp(l.Y, r.Y));
        }
       
        public static PointF Map(PointF a, Func<float, float> map)
        {
            return new PointF(map(a.X), map(a.Y));
        }


        public static PointF Add(this PointF l, PointF r)
        {
            return CalcPairwise(l, r, (x, y) => x + y);
        }
        public static PointF Subtract(this PointF l, PointF r)
        {
            return CalcPairwise(l, r, (x, y) => x - y);
        }

        public static PointF Multiply(this PointF a, float scalar)
        {
            return Map(a, x => x * scalar);
        }
        public static PointF Divide(this PointF a, float scalar)
        {
            return Map(a, x => x / scalar);
        }

        public static double DistanceTo(this PointF p1, PointF p2)
        {
            return Math.Sqrt(
                    Math.Pow(p2.X - p1.X, 2) +
                    Math.Pow(p2.Y - p1.Y, 2)
            );
        }
    }
}
