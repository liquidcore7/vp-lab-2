﻿using System;
using System.Linq;
using System.Drawing;

namespace Lab2
{
    public class Polygon
    {

        public readonly PointF[] Vertices;
        public readonly PointF Center;


        private static PointF nextPoint(PointF last, double sideLength, double angleDelta)
        {
            return new PointF(
                last.X + (float) (Math.Cos(angleDelta) * sideLength),
                last.Y - (float) (Math.Sin(angleDelta) * sideLength)
            );
        }


        private static PointF[] createVertices(PointF center, double radius, int verticesCount)
        {
            double adjacentRadiusesAngle = 2.0 * Math.PI / verticesCount;
            // by cosine rule
            double sideLength = Math.Sqrt(2.0 * radius * radius * (1.0 - Math.Cos(adjacentRadiusesAngle)));

            PointF leftBottom = new PointF(
                center.X - (float) (sideLength / 2),
                center.Y + (float) (radius * Math.Cos(adjacentRadiusesAngle / 2.0))
            );

            PointF[] points = new PointF[verticesCount];
            points[0] = leftBottom;
            points[1] = new PointF(leftBottom.X + (float) sideLength, leftBottom.Y);

            for (int i = 2; i < verticesCount; ++i)
            {
                points[i] = nextPoint(points[i - 1], sideLength, adjacentRadiusesAngle * (i - 1));
            }

            return points;
        }

        private Polygon(PointF center, PointF[] vertices)
        {
            this.Center = center;
            this.Vertices = vertices;
        }


        public Polygon(PointF center, double radius, int verticesCount)
        {
            this.Center = center;
            this.Vertices = createVertices(center, radius, verticesCount);
        }

        public Polygon(PointF center, PointF offset, int minDimension, int percentage, int verticesCount) : this(
            center.Add(offset),
            minDimension * percentage / 200.0,
            verticesCount
        ) {  }


        public Polygon RotatedBy(float radians)
        {
            float sin = (float) Math.Sin(radians);
            float cos = (float) Math.Cos(radians);

            var rotatedPoints = Vertices.Select(point =>
            {
                var radVector = point.Subtract(Center);
                float distanceToCenter = (float) Center.DistanceTo(point);

                var normalizedVector = new PointF(radVector.X / distanceToCenter, radVector.Y / distanceToCenter);
                var rotated = new PointF(normalizedVector.X * cos - normalizedVector.Y * sin,
                                         normalizedVector.X * sin + normalizedVector.Y * cos);

                return Center.Add(new PointF(rotated.X * distanceToCenter, rotated.Y * distanceToCenter));
            }).ToArray();

            return new Polygon(Center, rotatedPoints);
        }

        public Polygon Moved(PointF from, PointF to)
        {
            if (!ContainsPoint(from))
            {
                return this;
            }
            int movedVerticeIndex = Vertices.ToList().FindIndex(v => v.DistanceTo(from) < 5.0);
            var delta = to.Subtract(from);

            if (movedVerticeIndex == -1)
            {
                return new Polygon(Center.Add(delta), Vertices.Select(v => v.Add(delta)).ToArray());
            } else
            {
                var verticesCopy = Vertices.Clone() as PointF[];
                verticesCopy.SetValue(Vertices[movedVerticeIndex].Add(delta), movedVerticeIndex);
                return new Polygon(Center, verticesCopy);
            }
        }

        public Polygon Resized(int oldPercentage, int newPercentage)
        {
            return new Polygon(
                Center,
                Vertices
                    .Select(v => v.Subtract(Center))
                    .Select(radiusV => radiusV.Multiply((float) newPercentage / oldPercentage))
                    .Select(v => v.Add(Center))
                    .ToArray()
            );
        }


        public bool ContainsPoint(PointF point)
        {
            for (int i = 1; i < Vertices.Length; ++i)
            {
                var pathBegin = Vertices[i - 1];
                var pathEnd = Vertices[i];

                float crossProduct = (pathBegin.Y - point.Y) * (pathEnd.X - pathBegin.X) -
                    (point.X - pathBegin.X) * (pathBegin.Y - pathEnd.Y);

                if (crossProduct < 0) // point is on the right side relative to segment
                    return false;
            }
            return true;
        }

        public double GetMaxRadius()
        {
            return Vertices
                .Select(v => v.DistanceTo(Center))
                .Max();
        }
    }
}
