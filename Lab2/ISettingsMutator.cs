﻿using System;
namespace Lab2
{
    public interface ISettingsMutator
    {
        void ApplyToPolygon(ref PolygonDrawing old);
        void ApplySettings(PolygonDrawing drawing);
        void OnApply(EventHandler handler);
    }
}
